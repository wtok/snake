#include <curses.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "snake.h"
#include "util.h"

/*** struct point utilities ***/

/* Are the two given points the same? */
inline bool
same_point(struct point p1, struct point p2)
{
	return (p1.x == p2.x && p1.y == p2.y);
}

/* Manhattan/Taxicab (non-diagonal) distance between points. */
inline int
manhattan_distance(struct point p1, struct point p2)
{
	return abs(p2.x - p1.x) + abs(p2.y - p1.y);
}

/* Is the given point on any edge of the map? */
inline bool
point_is_within_rect(struct point p, struct rect r)
{
	return ((p.x >= r.x && p.x <= (r.x + r.w - 1))
		&& (p.y >= r.y && p.y <= (r.y + r.h - 1)));
}

/* Move pt to a random, valid position within the
 * but it won't be on the map borders. */
inline struct point
random_point_within_rect(struct rect r)
{
	return (struct point){.x = r.x + (rand() % (r.w - 1)) + 1,
			      .y = r.y + (rand() % (r.h - 1)) + 1};
}

/* Move given point one unit in the given direction. */
inline void
move_point(struct point p[static 1], int direction)
{
	switch (direction) {
	case 'w':
	case KEY_UP: p->y -= 1; break;
	case 'a':
	case KEY_LEFT: p->x -= 1; break;
	case 's':
	case KEY_DOWN: p->y += 1; break;
	case 'd':
	case KEY_RIGHT: p->x += 1; break;
	default: break;
	}
}

/*** struct snake utilities ***/

/* Is this point the snake? */
inline bool
point_is_snake(struct point p, struct snake s[static 1])
{
	for (int i = 0; i < s->length; ++i)
		if (same_point(p, s->history[i])) return true;
	return false;
}

/* Identical to above, but does not include the head. */
inline bool
point_is_tail(struct point p, struct snake s[static 1])
{
	for (int i = 1; i < s->length; ++i)
		if (same_point(p, s->history[i])) return true;
	return false;
}

/* Move all the snake's tail pieces up one place, then move its head one unit in
 * the given direction. */
void
move_snake(struct snake s[static 1], int direction)
{
	for (size_t i         = s->length; i > 0; --i)
		s->history[i] = s->history[i - 1];
	move_point(&s->history[0], direction);
}

/* Will pathing to this point lose the game? */
inline bool
point_is_unpathable(struct point p, struct rect r, struct snake s[static 1])
{
	return (!point_is_within_rect(p, r) || point_is_tail(p, s));
}

/*** Directions ***/

/* Is this integer a valid direction? */
inline bool
input_is_direction(int input)
{
	switch (input) {
	case 'w':
	case 'a':
	case 's':
	case 'd':
	case KEY_LEFT:
	case KEY_UP:
	case KEY_RIGHT:
	case KEY_DOWN: return true;
	default: return false;
	}
}

/* Return the opposite direction of input */
inline int
opposite_direction(int direction)
{
	int opp = ERR;
	switch (direction) {
	case 'w': opp       = 's'; break;
	case 'a': opp       = 'd'; break;
	case 's': opp       = 'w'; break;
	case 'd': opp       = 'a'; break;
	case KEY_UP: opp    = KEY_DOWN; break;
	case KEY_LEFT: opp  = KEY_RIGHT; break;
	case KEY_DOWN: opp  = KEY_UP; break;
	case KEY_RIGHT: opp = KEY_LEFT; break;
	}
	return opp;
}

/* Return a good direction to go in order to get from 'from' to 'to'. */
inline int
direction_of_point(struct point from, struct point to)
{
	if (from.x > to.x) return KEY_RIGHT;
	if (from.x < to.x) return KEY_LEFT;
	if (from.y > to.y) return KEY_DOWN;
	if (from.y < to.y) return KEY_UP;
	return KEY_END; /* TODO: Error handling */
}

/*** Miscellaneous ***/

/* Did the player win the game? */
inline bool
player_won_the_game(struct snake *snake)
{
	return (snake->length == MAX_SNAKE_LENGTH);
}

/* Clear a window and put a border around it, so it's ready to print to. */
inline void
blank_window(WINDOW window[static 1])
{
	wclear(window);
	wborder(window, '|', '|', '-', '-', '+', '+', '+', '+');
}
