#pragma once

#include <curses.h>

/* Screen size */
#define SCREEN_WIDTH 24
#define SCREEN_HEIGHT 10
#define SCREEN_AREA (SCREEN_WIDTH * SCREEN_HEIGHT)

/* Area of the playable section of the screen (i.e., excluding borders) */
#define MAP_WIDTH (SCREEN_WIDTH - 2)
#define MAP_HEIGHT (SCREEN_HEIGHT - 2)
#define MAP_AREA (MAP_WIDTH * MAP_HEIGHT)

/* Snake-specific */
#define MAX_SNAKE_LENGTH MAP_AREA

/* Characters used to represent game elements */
#define FRUIT_CHAR '*'
#define TAIL_CHAR 'o'
#define HEAD_CHAR '@'

/* Button pressed to quit */
#define INPUT_QUIT 'q'

/* a point in 2d space */
struct point {
	int x, y;
};

/* a rectangle in 2d space */
struct rect {
	int x, y, w, h;
};

/* a player-controlled snake */
struct snake {
	/* History of snake's locations. history[0] is the current location of
	   the snake's head. */
	struct point history[MAX_SNAKE_LENGTH];
	/* Length of snake, including head. */
	int length;
	/* Direction snake is facing. Represented by either KEY_UP, KEY_RIGHT,
	   KEY_DOWN, or KEY_LEFT. */
	int facing;
};
