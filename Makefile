def_flags=-D_POSIX_C_SOURCE=200809UL
std_flags=-std=c99 -Wpedantic
err_flags=-Wall -Wextra -Werror
lnk_flags=-lncurses

all_flags=$(def_flags) $(std_flags) $(err_flags) $(lnk_flags)

CC_CALL=$(CC) $(all_flags)

sources=snake.c util.c
headers=snake.h util.h
all_files=$(sources) $(headers)

all: $(all_sources)
	$(CC_CALL) $(sources) -o snake

debug: $(all_sources)
	$(CC_CALL) $(sources) -o snake -g

profile: $(all_sources)
	$(CC_CALL) $(sources) -o snake -g -pg
