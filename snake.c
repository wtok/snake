/* Walter O'Keefe
 * Project started 2015-07-22
 * Snake in ncurses */

#include <assert.h>
#include <curses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "snake.h"
#include "util.h"

/* Determines game speed. 15 or so is probably best. */
#define DEFAULT_FPS 15
static unsigned int FRAMES_PER_SECOND = DEFAULT_FPS;
static unsigned int NANOSECONDS_PER_FRAME
    = ((1 * 1000 * 1000 * 1000) / DEFAULT_FPS);

/* Times per-frame to check for input in wait_for_input(). */
#define INPUT_CHECKS_PER_FRAME 2

float               win_percent;
struct point        fruit;
struct snake        mr_snake = {.length = 1};
struct point *const head   = &mr_snake.history[0];
struct rect const MAP_RECT = {.x = 1, .y = 1, .w = MAP_WIDTH, .h = MAP_HEIGHT};

/* Wait 'ns' nanoseconds for input from 'window'. If none, return ERR. */
static int
wait_for_input(WINDOW window[static 1], int ns)
{
	int input = ERR;

	int const interval = NANOSECONDS_PER_FRAME / INPUT_CHECKS_PER_FRAME;
	struct timespec ts = {.tv_sec = 0, .tv_nsec = 0};

	while (ts.tv_nsec < ns) {
		if (input == ERR) input = wgetch(window);
		ts.tv_nsec += interval;
		nanosleep(&ts, NULL);
	}

	return input;
}

static inline char
get_snake_head_char(int facing)
{
	switch (facing) {
	case 'w':
	case KEY_UP: return '^';
	case 'd':
	case KEY_RIGHT: return '>';
	case 's':
	case KEY_DOWN: return 'v';
	case 'a':
	case KEY_LEFT: return '<';
	default: return '@';
	}
}

static int
main_play(void)
{
	/* This function uses a goto (main_play_cleanup) for return, so it can
	 * clean up ncurses nonsense. */
	int ret;

	/* Init ncurses windows */
	initscr();
	WINDOW *gamew = newwin(SCREEN_HEIGHT, SCREEN_WIDTH, 0, 0);
	WINDOW *infow = newwin(3, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
	char    infostr[MAP_WIDTH]; /* string placed in the infobox */

	/* ncurses boilerplate */
	noecho();
	cbreak();
	curs_set(0);
	keypad(gamew, 1);
	nodelay(gamew, 1); // we implement our own idle loop

	int input = ERR; /* input read from keyboard (KEY_* from curses.h) */

	/* insert_coin() */
	{
		blank_window(gamew);
		blank_window(infow);

		*head = random_point_within_rect(MAP_RECT);
		fruit = random_point_within_rect(MAP_RECT);

		mvwaddch(gamew, head->y, head->x,
			 get_snake_head_char(mr_snake.facing));
		mvwaddch(gamew, fruit.y, fruit.x, FRUIT_CHAR);

		sprintf(infostr, "Press a key to start");
		mvwaddstr(infow, 1, 2, infostr);

		wrefresh(gamew);
		wrefresh(infow);

		while (!(input_is_direction(input) || input == INPUT_QUIT)) {
			input = wait_for_input(gamew, NANOSECONDS_PER_FRAME);
		}

		if (input == INPUT_QUIT) {
			ret = -1;
			goto main_play_cleanup;
		} else {
			mr_snake.facing = input;
			move_snake(&mr_snake, mr_snake.facing);
		}
	}

	/* Game loop */
	while (true) {
		/* ---------------------- */
		/* --- render_frame() --- */
		/* ---------------------- */

		blank_window(gamew);
		blank_window(infow);

		/* render_head() */

		mvwaddch(gamew, head->y, head->x,
			 get_snake_head_char(mr_snake.facing));

		/* render_tail() */
		for (int i = 1; i < mr_snake.length; ++i) {
			struct point pt = mr_snake.history[i]; // brevity
			mvwaddch(gamew, pt.y, pt.x, TAIL_CHAR);
		}

		/* render_fruit() */
		mvwaddch(gamew, fruit.y, fruit.x, FRUIT_CHAR);

		/* calculate_game_info() */
		sprintf(infostr, "%.2f%% | %s", win_percent, keyname(input));

		/* render_game_info() */
		mvwaddstr(infow, 1, 2, infostr);

		/* draw_to_screen() */
		wrefresh(gamew);
		wrefresh(infow);

		/* ------------------------- */
		/* --- calculate_frame() --- */
		/* ------------------------- */

		/* Grab input */
		input = wait_for_input(gamew, NANOSECONDS_PER_FRAME);

		/* Input from the player doesn't move the snake, it just sets
		 * where it's facing. */
		switch (input) {
		case 'w':
		case 'a':
		case 's':
		case 'd':
		case KEY_UP:
		case KEY_RIGHT:
		case KEY_DOWN:
		case KEY_LEFT:
			if (input != opposite_direction(mr_snake.facing))
				mr_snake.facing = input;
			break;
		case INPUT_QUIT: ret = -1; goto main_play_cleanup;
		default: break;
		}

		move_snake(&mr_snake, mr_snake.facing);

		/* Evaluate consequences of movement */

		/* Did the snake eat a fruit? */
		if (same_point(*head, fruit)) {
			do {
				fruit = random_point_within_rect(MAP_RECT);
			} while (point_is_snake(fruit, &mr_snake)
				 || !point_is_within_rect(fruit, MAP_RECT));
			mr_snake.length += 1;
			win_percent
			    = 100 * (mr_snake.length / (float) MAP_AREA);
		}

		/* Did the snake hit anything dangerous? */
		if (point_is_unpathable(*head, MAP_RECT, &mr_snake)) {
			ret = mr_snake.length;
			goto lose_the_game;
		}

		/* Did the snake win the game? */
		if (player_won_the_game(&mr_snake)) {
			ret = mr_snake.length;
			goto main_play_cleanup;
		}
	}
/* end of game loop */

lose_the_game:
	/* Do a little animation */
	for (int i = 3; i != 0; --i) {
		mvwaddch(gamew, mr_snake.history[1].y, mr_snake.history[1].x,
			 TAIL_CHAR);

		blank_window(infow);
		sprintf(infostr, "You died! X_X");
		mvwaddstr(infow, 1, 2, infostr);
		wrefresh(infow);

		struct timespec anim_frame_ts
		    = {.tv_sec = 0, .tv_nsec = 4 * NANOSECONDS_PER_FRAME};

		mvwaddch(gamew, head->y, head->x, 'X');
		wrefresh(gamew);
		nanosleep(&anim_frame_ts, NULL);

		mvwaddch(gamew, head->y, head->x,
			 get_snake_head_char(mr_snake.facing));
		wrefresh(gamew);
		nanosleep(&anim_frame_ts, NULL);
	}

main_play_cleanup:
	endwin();
	return ret;
}

/* main() sets up options and does post-game cleanup */
int
main(int argc, char *argv[])
{
	if (argc == 2) {
		/* Set the FPS, if given. */
		sscanf(argv[1], "%u", &FRAMES_PER_SECOND);
		NANOSECONDS_PER_FRAME
		    = ((1 * 1000 * 1000 * 1000) / FRAMES_PER_SECOND);
	}

	srand((int) time(NULL));

	/* Play the game */
	int         length = main_play();
	char const *message;

	switch (length) {
	case MAP_AREA: message = "You won! Congratulations!"; break;
	case -1: message       = "Bye!"; break;
	default: {
		float win_percent = 100 * (mr_snake.length / (float) MAP_AREA);
		if (win_percent < 15)
			message = "Well, nice try anyway.";
		else if (win_percent < 50)
			message = "Well done!";
		else if (win_percent > 85)
			message = "So close!";
		break;
	}
	}

	printf("%s\n", message);
	printf("Your score was %d.\n", mr_snake.length);
	return 0;
}
