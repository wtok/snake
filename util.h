#pragma once

#include <curses.h>

#include "snake.h"

/* point utilities */
bool same_point(struct point p1, struct point p2);
int manhattan_distance(struct point p1, struct point p2);
bool point_is_within_rect(struct point p, struct rect r);
struct point random_point_within_rect(struct rect r);
void move_point(struct point p[static 1], int direction);

/* snake utilities */
bool point_is_snake(struct point p, struct snake s[static 1]);
bool point_is_tail(struct point p, struct snake s[static 1]);
bool point_is_unpathable(struct point p, struct rect r, struct snake s[static 1]);
void move_snake(struct snake s[static 1], int direction);

/* directions */
bool input_is_direction(int input);
int opposite_direction(int direction);
int direction_of_point(struct point from, struct point to);

/* game logic */
bool player_won_the_game(struct snake s[static 1]);

/* reset a window */
void blank_window(WINDOW window[static 1]);
